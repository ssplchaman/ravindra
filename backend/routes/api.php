<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdminCategory;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/login',[AdminController::class,'adminLogin']);
Route::post('/addcategory',[AdminCategory::class , 'addCategory']);  // insert data into category
Route::get('/display',[AdminCategory::class , 'display']);  // show data from category
Route::delete('/delete/{id}',[AdminCategory::class , 'delete']); //  delete data from category
Route::get('/edit/{id}',[AdminCategory::class , 'editCategory']);
Route::put('/update/{id}',[AdminCategory::class , 'updateCategory']);