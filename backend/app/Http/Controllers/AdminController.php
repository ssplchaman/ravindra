<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
class AdminController extends Controller
{
    function adminLogin(Request $req)
    {
        $admin =  Admin::where('email',$req->email)->first();
        if(!$admin)
        {
            return response(
                ['error'=>["Email not matched"]]
            );
        }
        elseif($req->password != $admin->password)
        {
            return response(
                ['error'=>["Password not matched"]]
            );
        }
        return $admin;
        //return "Hello admin";
    }
}
