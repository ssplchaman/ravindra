<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class AdminCategory extends Controller
{
    function addCategory(Request $req){
        //return "category added";
        //Add products into the database
        $insert = new Category;
        $insert->category_name = $req->input('name');    // $req->input('name')  ka name or useState ka name same hona chaiye.
        $insert->category_type = $req->input('type');
        $insert->category_subtype = $req->input('subtype');
        $insert->isActive = $req->input('isactive');
        $insert->save();  //return path of file store
        return $insert;
        //return $req;
    }

    function display(){
        return Category::all();
    }

    function delete($id)
    {
        //return $id;
        $delete = Category::where('id',$id)->delete();
        if($delete)
        {
            return ["result"=>"Category Item has been deleted"];
        }
        else
        {
            return ["result"=>"Delete Operation Failed"];
        }
    }

    function editCategory($id)
    {
        //return $id;
        return Category::find($id);  // geting all data from category where id  = $id
    }

    function updateCategory($id,Request $req){
       // return $id;
       $update = Category::find($id);
       $update->category_name = $req->input('name');    // $req->input('name')  ka name or useState ka name same hona chaiye.
       $update->category_type = $req->input('type');
       $update->category_subtype = $req->input('subtype');
       $update->isActive = $req->input('isactive');
       return $update->save();


    }
}
