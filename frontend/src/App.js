import { BrowserRouter,Route } from "react-router-dom";
import AdminLogin from "./components/admin/AdminLogin";
import Category from "./components/admin/Category";
import Product from "./components/admin/Product";
import Sidebar from "./components/admin/Sidebar";
import Update from "./components/admin/Update";

function App() {

  return (
    <>
    <BrowserRouter>
    <Route exact path="/admin" component={AdminLogin}></Route>
    <Route exact path="/dashboard" component={Sidebar}></Route>
    <Route exact path="/category" component={Category}></Route>
    <Route exact path="/product" component={Product}></Route>
    <Route exact path="/update/:id" component={Update}></Route>
    </BrowserRouter>
    </>
    
  );
}


export default App;
