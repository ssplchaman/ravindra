import React,{useState , useEffect} from 'react'
import '../../css/mycss.css'
import {useHistory} from 'react-router-dom';

export default function AdminLogin() {

    useEffect(()=>{
        if(localStorage.getItem('user_info'))
        {
            history.push("/dashboard");
        }
    },[]);
    const history = useHistory();
    //console.log(history)

    const[email,setEmail] = useState();
    const[password,setPassword] = useState();

    async function login(e){
        e.preventDefault();  //if btn type is submit
       //alert(`hello ${email} and ${password}`);

       let item = {email,password}
       let result = await fetch('http://127.0.0.1:8000/api/login',{
            method : 'POST',
            headers : {
                "Content-Type":"application/json",
                "Accept":"application/json"
            },
            body : JSON.stringify(item)
        });

        result = await result.json();
        console.log(result.name)
        if(email === result.email)
        {
            console.log("valid user");
            localStorage.setItem('user_info',JSON.stringify(result));
            history.push("./dashboard");
            //window.location = "./add";
        }
        else{
            alert("Login with valid information");
            console.log("Login with valid information");
        }
        
    }
    
    return (
        <>           
        <div className="container">
        <div className="row">
            <div className="col-lg-3 col-md-2"></div>
            <div className="col-lg-6 col-md-8 login-box">
                <div className="col-lg-12 login-key">
                    <i className="fa fa-key" aria-hidden="true"></i>
                </div>
                <div className="col-lg-12 login-title">
                    ADMIN PANEL
                </div>

                <div className="col-lg-12 login-form">
                    <div className="col-lg-12 login-form">
                        <form>
                            <div className="form-group">
                                <label className="form-control-label">EMAIL</label>
                                <input type="text" className="form-control email" onChange={(e)=>setEmail(e.target.value)}/>
                            </div>
                            <div className="form-group">
                                <label className="form-control-label">PASSWORD</label>
                                <input type="password" className="form-control pass" onChange={(e)=>setPassword(e.target.value)}/>
                            </div>

                            <div className="col-lg-12 loginbttm">
                                <div className="col-lg-6 login-btm login-text">
                        
                                </div>
                                <div className="col-lg-6 login-btm login-button">
                                    {/* <button type="submit" className="btn btn-outline-primary" onClick={login}>LOGIN</button> */}
                                    <button className="btn btn-primary my-4" onClick={login}>Login</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="col-lg-3 col-md-2"></div>
            </div>
        </div>
        </div>
        </>
    )
}
