import React, { useState , useEffect } from 'react';
import { withRouter , useHistory } from 'react-router-dom';

 function Update(props) {
   const history = useHistory();
    const[data,setData] = useState([]);
    // 2 defining states for storing new /updated data
    const [name, setName] = useState();
    const [type, setType] = useState();
    const [subtype, setSubtype] = useState();
    const [isactive, setIsactive] = useState();
    //console.log(props.match.params.id);  fetchind id value that is pass in parameter from table
    useEffect(async() => {
        let result = await fetch("http://127.0.0.1:8000/api/edit/"+props.match.params.id);
        result = await result.json();
       // console.log(result.category_name)  fetching database values in result
        setData(result);
        // 3 setting new/updated data into states
        setName(result.category_name);
        setType(result.category_type);
        setSubtype(result.category_subtype);
        setIsactive(result.isActive);
        // 4 iske baad hum onChange me new values ko store karwaenge 
    }, []);

    async function UpdateCat(id){
      //console.log(id)   // 1 fetching category id to update record
      // 5 insert updated record into the database backend using  api
      let item = { name, type, subtype, isactive };
      let result = await fetch("http://127.0.0.1:8000/api/update/"+id+"?_method=PUT", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify(item),
      });
      result = await result.json();
      console.log(result);
      history.push("../category");
    }
    return (
        <>
        <h1 className="my-4 mx-4">Update Category </h1>
        <div className="col-sm-6 offset-sm-3 mt-4">
          <label>Category-Name</label>
          <input
          style={{ backgroundColor: "#fff", color: "#000" }}
          type="text"
          className="form-control w-50 my-2" onChange={(e)=>setName(e.target.value)} defaultValue={data.category_name}
        />
        <label>Category-Type</label>
        <input
        style={{ backgroundColor: "#fff", color: "#000" }}
          type="text"
          className="form-control w-50 my-2" onChange={(e)=>setType(e.target.value)} defaultValue={data.category_type}
        />
        <label>Category-Subtype</label>
        <input
        style={{ backgroundColor: "#fff", color: "#000" }}
          type="text"
          className="form-control w-50 my-2" onChange={(e)=>setSubtype(e.target.value)} defaultValue={data.category_subtype}
        />
        <label>Category-isActive</label>
        <select onChange={(e) => setIsactive(e.target.value)} className="form-select w-50 my-2" aria-label="Default select example">
          <option selected>{data.isActive}</option>
          <option>1</option>
          <option>0</option>
        </select>

        <button className="btn btn-success" onClick={()=>UpdateCat(data.id)}>Update</button>
        </div>
        </>
    )
}

export default withRouter(Update);