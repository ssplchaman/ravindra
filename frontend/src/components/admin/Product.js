import React,{useEffect} from 'react'
import Sidebar from './Sidebar'
import CategoryModal from './CategoryModal'
import Table from './Table'
import { useHistory } from 'react-router'

export default function Product() {
    const history = useHistory();
    useEffect(()=>{
        if(!localStorage.getItem('user_info'))
        {
            history.push("/admin");
        }
    },[])

    return (
        <>
            <Sidebar/>
            <div style={{position:"relative",left:"10%"}} className="container">
            <h1>Product Page</h1> 
            {/* <CategoryModal name="Add Product"/> */}
            {/* <Table/> */}
            </div>     
        </>
    )
}
