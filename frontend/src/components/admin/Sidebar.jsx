import React,{useEffect} from "react";
import "../../css/sidebar.css";
import { NavDropdown } from "react-bootstrap";
import {useHistory} from 'react-router-dom';
import {Link} from 'react-router-dom';

export default function Sidebar() {

    useEffect(()=>{
        if(!localStorage.getItem('user_info'))
        {
            history.push("/admin");
        }
    },[])


    const adminInfo = JSON.parse(localStorage.getItem('user_info'));  // convert json format data of localstorage into js object
    //console.log(adminInfo)
    const history = useHistory();
    function logout(){
        localStorage.clear();
        history.push("./admin");
    }
  return (
    <>
      <div className="row">
        <div className="sidebar col-sm-3">
        <Link to="/dashboard">Home</Link>
        <Link to="/category">Category</Link>
        <Link to="/product">Product</Link>
        </div>

        <div className="col-sm-7 offset-sm-3">
          <h2 className="my-2">Welcome To Admin Dashboard</h2>
        </div>
        <div className=" col-sm-2">
        {localStorage.getItem('user_info') ? 
          <NavDropdown title={adminInfo.name}>
            <NavDropdown.Item onClick={logout}>Logout</NavDropdown.Item>
          </NavDropdown>
            : null}
        </div>
      </div>
    </>
  );
}
