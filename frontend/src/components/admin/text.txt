Category.js


import React,{useState,useEffect} from 'react'
import Sidebar from './Sidebar'
import CategoryModal from './CategoryModal'
import Table from './Table'
import { useHistory } from 'react-router'

export default function Category() {

    const history = useHistory();
    useEffect(()=>{
        if(!localStorage.getItem('user_info'))
        {
            history.push("/admin");
        }
    },[])
    return (
        <>
            <Sidebar/>
            <div style={{position:"relative",left:"10%"}} className="container">
            <h1>Category Page</h1> 
            <CategoryModal/>
            <Table />
            </div>  
        </>
    )
}


Category-Modal . js
import React, { useState } from "react";
import { useHistory } from "react-router";
import Button from "@restart/ui/esm/Button";
import { Modal } from "react-bootstrap";


export default function CategoryModal() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const history = useHistory();
  const[name,setName] = useState();
  const[type,setType] = useState();
  const[subtype,setSubtype] = useState();
  const[isactive,setIsactive] = useState();

  async function addCategory({}){
   // console.log(name,type,subtype,isactive)   // fetching data from form
   // call api to send data
   let item = {name,type,subtype,isactive}
   let result = await fetch("http://127.0.0.1:8000/api/addcategory",{
     method : "POST",
     headers : {
      "Content-Type":"application/json",
      "Accept":"application/json"
     },
     body : JSON.stringify(item)
   });
   result = await result.json();
   console.log(result);
   setShow(false);
 }
  return (
    <>
      <Button
        variant="primary"
        className="btn btn-primary"
        onClick={handleShow}
      >
        Add Category
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add Category</Modal.Title>
        </Modal.Header>

        <Modal.Body></Modal.Body>
        <input
          style={{ backgroundColor: "#fff", color: "#000" }}
          type="text"
          className="form-control"
          placeholder="Category Name"
          onChange={(e) => setName(e.target.value)}
        />
        <input
          style={{ backgroundColor: "#fff", color: "#000" }}
          type="text"
          className="form-control"
          placeholder="Category Type"
          onChange={(e) => setType(e.target.value)}
        />
        <input
          style={{ backgroundColor: "#fff", color: "#000" }}
          type="text"
          className="form-control"
          placeholder="Category Sub-Type"
          onChange={(e) => setSubtype(e.target.value)}
        />
        <input
          style={{ backgroundColor: "#fff", color: "#000" }}
          type="text"
          className="form-control"
          placeholder="IsActive"
          onChange={(e) => setIsactive(e.target.value)}
        />
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={addCategory}>
            Insert Category
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

// export default function CategoryModal() {
//   const history = useHistory();
//   const[name,setName] = useState();
//   const[type,setType] = useState();
//   const[subtype,setSubtype] = useState();
//   const[isactive,setIsactive] = useState();

//   async function addCategory({}){
//    // console.log(name,type,subtype,isactive)   // fetching data from form
//    // call api to send data
//    let item = {name,type,subtype,isactive}
//    let result = await fetch("http://127.0.0.1:8000/api/addcategory",{
//      method : "POST",
//      headers : {
//       "Content-Type":"application/json",
//       "Accept":"application/json"
//      },
//      body : JSON.stringify(item)
//    });
//    result = await result.json();
//    console.log(result);
//  }

//   return (
//     <>
//       <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
//        Add Category
//       </button>

//       <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
//         <div className="modal-dialog">
//           <div className="modal-content">
//             <div className="modal-header">
//               <h5 className="modal-title" id="exampleModalLabel">
//                 Add Category
//               </h5>
//               <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
//             </div>

//             <div className="modal-body">
//                 <input style={{backgroundColor:"#fff",color:"#000"}} type="text" className="form-control" placeholder="Category Name" onChange={(e)=>setName(e.target.value)}/>
//                 <input style={{backgroundColor:"#fff",color:"#000"}} type="text" className="form-control" placeholder="Category Type" onChange={(e)=>setType(e.target.value)}/>
//                 <input style={{backgroundColor:"#fff",color:"#000"}} type="text" className="form-control" placeholder="Category Sub-Type" onChange={(e)=>setSubtype(e.target.value)}/>
//                 <input style={{backgroundColor:"#fff",color:"#000"}} type="text" className="form-control" placeholder="IsActive" onChange={(e)=>setIsactive(e.target.value)}/>
//             </div>

//             <div className="modal-footer">
//               <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" >
//                 Close
//               </button>
//               <button type="button" className="btn btn-primary" onClick={addCategory}>
//                 Insert Category
//               </button>
//             </div>
//           </div>
//         </div>
//       </div>
//     </>
//   )
// }



Table.js

import React, { useState, useEffect } from "react";

export default function Table() {
  const [data, setData] = useState([]);
  useEffect(async () => {
    let result = await fetch("http://127.0.0.1:8000/api/display");
    result = await result.json();
    setData(result);
  }, []);
  console.log(data);

  return (
    <>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">id</th>
            <th scope="col">name</th>
            <th scope="col">type</th>
            <th scope="col">subtype</th>
            <th scope="col">active</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item) => (
            <tr>
              <th scope="row">{item.id}</th>
              <td>{item.category_name}</td>
              <td>{item.category_type}</td>
              <td>{item.category_subtype}</td>
              <td>{item.isActive}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}
