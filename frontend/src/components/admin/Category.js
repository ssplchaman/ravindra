import React,{useState,useEffect} from 'react'
import Sidebar from './Sidebar'
import CategoryModal from './CategoryModal'
import Table from './Table'
import { useHistory } from 'react-router'

export default function Category() {
    const [data, setData] = useState([]);

    const history = useHistory();

    //getData();
    useEffect(()=>{
        if(!localStorage.getItem('user_info'))
        {
            history.push("/admin");
        }
        getData();
       
    },[])

    async function getData(){
        let result = await fetch("http://127.0.0.1:8000/api/display");
        result = await result.json();
        setData(result);
    }
    return ( 
        <>
            <Sidebar/>
            <div style={{position:"relative",left:"10%"}} className="container">
            <h2>Category Page</h2> 
            <CategoryModal getCategories={getData}/>  
            <Table categories={data} getCategories={getData} />
            </div> 
        </>
    )
}


