import React, { useState, useEffect } from "react";
import '../../css/mycss.css';
import {Link} from 'react-router-dom';

export default function Table(props,{getCategories}) {
//console.log(props);     // return category details from data in Category.js
//console.log(props.getCategories)   // return function getData from Category.js
  const {categories} = props;  //getting props data from category.js
  async function deleteCat(id){
    console.log(id)  // fetching id of category table row field.
    // deleting records using api.
    let result = await fetch("http://127.0.0.1:8000/api/delete/"+id,{
          method : "DELETE"
      });
    result = result.json();
    props.getCategories();

  }
  return (
    <>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">id</th>
            <th scope="col">name</th>
            <th scope="col">type</th>
            <th scope="col">subtype</th>
            <th scope="col">active</th>
            <th colSpan="2" scope="col">Operations</th>
          </tr>
        </thead>
        <tbody>
          {categories.map((item, index) => (
            <tr key={index}>
              <th scope="row">{item.id}</th>
              <td>{item.category_name}</td>
              <td>{item.category_type}</td>
              <td>{item.category_subtype}</td>
              <td>{item.isActive}</td>
              <td><span onClick={()=>{deleteCat(item.id)}} className="delete">Delete</span></td>
              <td><Link to={"update/"+item.id}><span className="update">Update</span></Link></td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}
