import React from 'react'
//import '../css/style.css';
// import '../js/main.js';
// import '../css/slick.css';
// import '../css/bootstrap-select.min.css';
// import '../css/magnific-popup.css';
// import '../css/layers.css';
export default function Header() {
    return (
      <>
      <header className="header">
        <div className="row mt-3">
          <div className="col-lg-2 col-md-2"></div>
            <div class="alert alert-light col-lg-8 col-md-8" role="alert">
                460 West 34th Street, 15th floor, New York  -  Hotline: 804-377-3580 - 804-399-3580
            </div>
            <div className="col-lg-2 col-md-2">
              <button type="button" className="btn btn-success">Login/Register</button>
            </div>
        </div>

      </header>
      </>
    )
}
